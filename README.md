# Import script for publications submitted to Pure

This utility script allows to automatically generate Markdown files for publications based on information from [Pure](https://pure.tue.nl) system.

## Requirements

- Python 3
- bibtexparser library
- slugify library

Required libraries can be installed via `pip`:

    pip install -r requirements.txt

## Usage

1. Export your research overview in BibTeX format from Pure: 
    ![Export image](./images/export.png )

2. Copy (or move) the exported file to the folder with the script.

2. Navigate to the folder with the script and run it:

        python3 pure_bib.py <filename.bib>

3. Generated files can be found in `generated-md` folder next to the script.

## Providing links for author's internal page

It is also possible to automatically add links to author's internal page using this script. In order to do that, add information about the author to the `settings.json` file. The file already contains an example to help you with it.

After editing the file run the script with an additional argument:

    python3 pure_bib.py <filename.bib> -s settings.json

**NB**: The script doesn't overwrite the files that are already generated. If you want the generated file to updated, remove currrent generated version.