import argparse
import bibtexparser as bparse
import calendar
import json
import textwrap

from os import path
from slugify import slugify

GEN_DIR = "generated-md"

def strip_braces(text_str):
    return text_str.replace("{","").replace("}", "")

def escape_quotes(text_str):
    return text_str.replace('"','\\"')

def write_title(bib_entry, md_file):
    md_file.write('title="{}"\n'.format(strip_braces(escape_quotes(bib_entry['title']))))

def write_abstract(bib_entry, md_file):
    abstract = bib_entry.get('abstract')
    if abstract:
        md_file.write('abstract="{}"\n'.format(strip_braces(escape_quotes(abstract))))
        md_file.write('abstract_short=""\n')

def write_published_at(bib_entry, md_file):
    if 'booktitle' in bib_entry:
        md_file.write('publication="{}"\n'.format(strip_braces(bib_entry['booktitle'])))
        md_file.write('publication_short = ""\n')
        md_file.write('selected = false\n')

    elif 'journal' in bib_entry:
        md_file.write('publication="{}"\n'.format(strip_braces(bib_entry['journal'])))
        md_file.write('publication_short = ""\n')
        md_file.write('selected = false\n')

def convert_month_to_int(month):
    try:
        month=int(month)
    except ValueError:
        month_to_int = {v.lower(): k for k,v in enumerate(calendar.month_abbr)}
        month = month_to_int[month]
    return month


def write_date(bib_entry, md_file):
    date = ""
    if 'year' in bib_entry:
        date += "{}".format(bib_entry['year'])
        if 'month' in bib_entry:
            month_str = (''.join(filter(str.isalnum, bib_entry['month']))).lower()
            date += "-{:02d}".format(convert_month_to_int(month_str))
            date += "-01"
        else:
            date += "-01-01"
    if date:
        md_file.write('date="{}"\n'.format(date))
    
def write_authors(bib_entry, md_file, authors_links):
    if 'author' in bib_entry:
        authors = strip_braces(bib_entry['author']).split(' and ')
        for author in authors:
            try:
                last_name, name = author.split(',')
                author = "{} {}".format(name, last_name)
                
                author_link = authors_links.get(author)

                md_file.write(
                    textwrap.dedent('''
                        [[authors]]
                            name = "{}"
                            is_member = {}
                            link = "{}"
                        '''.format(author, "true" if author_link else "false", author_link if author_link else ""))
                )
            except ValueError:
                author_link = authors_links.get(author)

                md_file.write(
                    textwrap.dedent('''
                    [[authors]]
                        name = "{}"
                        is_member = {}
                        link = "{}"
                    '''.format(author, "true" if author_link else "false", author_link if author_link else "")
                    )
                )

def populate_image_fields(md_file):
    md_file.write(
        textwrap.dedent('''
            image = ""
            image_preview = ""
            math = false
        '''
        )
    )

def populate_url_fields(md_file):
    md_file.write(
        textwrap.dedent('''
            url_code = ""
            url_dataset = ""
            url_pdf = ""
            url_project = ""
            url_slides = ""
            url_video = ""
        '''
        )
    )

def dump_entry_to_file(bib_entry, filename, authors_links):
    
    with open(filename, 'w') as md_file:
        md_file.write("+++\n")
        write_title(bib_entry, md_file)
        write_abstract(bib_entry, md_file)
        write_published_at(bib_entry, md_file)
        write_date(bib_entry, md_file)
        populate_image_fields(md_file)
        populate_url_fields(md_file)
        write_authors(bib_entry, md_file, authors_links)
        md_file.write("+++\n")

def process_entry(bib_entry, authors_links):
    md_filename = "{}/{}.md".format(GEN_DIR, slugify(bib_entry['title']))
    if path.exists(md_filename):
        print("File {} exists, skipping...".format(md_filename))
    else:
        dump_entry_to_file(bib_entry, md_filename, authors_links)    

def main(bibfile, settings):
    if settings:
        with open(settings, 'r') as settings:
            authors_links = json.load(settings)['authors']
    else:
        authors_links = {}
    with open(bibfile, 'r') as bib_file:
        bib_database = bparse.load(bib_file)
        for bib_entry in bib_database.entries:
            process_entry(bib_entry, authors_links)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("bibfile",
                    help="Path to bibfile with publications")
    parser.add_argument("-s", "--settings",
                    help="Path to file with links to author pages")
    args = parser.parse_args()

    main(args.bibfile, args.settings)